from pwn import log
from Crypto.Cipher import AES
import os, struct, hashlib

target_id = '356280a58d3c437a45268a0b226d8cccad7b5dd28f5d1b37abf1873cc426a8a5'

log.info('searching device id')
for i in xrange(9999999):
  device_id = '99999991%07d' % i
  hexhash = hashlib.sha256(device_id).hexdigest()
  if hexhash == target_id:
    log.success('found: {}'.format(device_id))
    break

log.info('decrypting png')

key = '!' + device_id
iv = 'kLwC29iMc4nRMuE5'
aes = AES.new(key, AES.MODE_CBC, iv)

decrypted = ''

with open('jewel_c.png', 'rb') as src:
  while True:
    buf = src.read(4096)
    if buf == '':
      break
    decrypted += aes.decrypt(buf)

with open('jewel_c_dec.png', 'wb') as dst:
  dst.write(decrypted)

log.success('finished')
