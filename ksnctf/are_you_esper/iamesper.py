from pwn import *

HOST = 'ctfq.sweetduet.info'
PORT = 10777

with remote(HOST, PORT, timeout=0.5) as r:
  esper = process('./esper')
  nums = esper.recvall()
  nums = nums.split()
  for i in range(20):
    r.sendline(nums[i])
  res = r.recvall()
  print res

if esper:
  esper.close()
