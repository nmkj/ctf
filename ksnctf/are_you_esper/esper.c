#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <x86_64-linux-gnu/gmp.h>

int main() {
  unsigned int now, rand_num;
  mpz_t imul_res, m_ecx, m_eax, m_temp, m_temp2;
  mpz_init(imul_res);
  mpz_init(m_ecx);
  mpz_init(m_eax);
  mpz_init(m_temp);
  mpz_init(m_temp2);

  now = time(NULL);
  srandom(now);
  int i;
  for (i = 0; i < 20; i++) {
    rand_num = rand();

    mpz_set_ui(m_eax, 0x66666667);
    mpz_set_ui(m_ecx, rand_num);
    mpz_mul(imul_res, m_eax, m_ecx);
    mpz_tdiv_q_ui(m_temp, imul_res, 0x100000000);
    unsigned int upper_res = mpz_get_ui(m_temp);
    mpz_mul_ui(m_temp, m_temp, 0x100000000);
    mpz_sub(m_temp2, imul_res, m_temp);
    unsigned int lower_res = mpz_get_ui(m_temp2);
    lower_res >>= 0x1f;
    upper_res >>= 2;
    unsigned int edi = upper_res * 5;
    /* unsigned int *eax = 0x80bb260; */
    edi *= 2;
    rand_num -= edi;

    printf("%d\n", rand_num);
  }

  return 0;
}

