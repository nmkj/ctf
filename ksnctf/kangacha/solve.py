import requests
import subprocess

URL = 'http://ctfq.sweetduet.info:10080/~q31/kangacha.php'

s = requests.Session()

r = s.post(URL, data={'submit': 'Gacha'})
ship = s.cookies['ship']
signature = s.cookies['signature']

cmd = 'hashpump -s {sig} -d {data} -k {keylen} -a {index}'
cmd = cmd.format(sig=signature, data=ship, keylen=21, index=',10')

out = subprocess.check_output(cmd.strip().split(' '))
new_sig, new_ship = out.decode('utf-8').strip().split('\n')
new_ship = new_ship.replace('\\x', '%')

s.cookies.clear()
args = {'domain': 'ctfq.sweetduet.info', 'path': '/~q31'}
s.cookies.set('ship', new_ship, **args)
s.cookies.set('signature', new_sig, **args)
r = s.get(URL)

print r.text
