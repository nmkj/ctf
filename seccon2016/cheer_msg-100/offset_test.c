#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  if (argc < 2) {
    exit(1);
  }

  int length = atoi(argv[1]);
  int offset;
  
  asm volatile (
    "mov    %%eax, %1;"
    "mov    %%eax,-0x10(%%ebp);"
    "mov    -0x10(%%ebp),%%eax;"
    "lea    0xf(%%eax),%%edx;"
    "mov    $0x10,%%eax;"
    "sub    $0x1,%%eax;"
    "add    %%edx,%%eax;"
    "mov    $0x10,%%ecx;"
    "mov    $0x0,%%edx;"
    "div    %%ecx;"
    "imul   $0x10,%%eax,%%eax;"
    "mov    %%eax, %0;"
    : "=g" (offset)
    : "g" (length)
  );

  printf("%d\n", offset);
  return 0;
}

