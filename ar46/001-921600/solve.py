def getdata(fname):
    with open(fname, 'rb') as f:
        f.seek(46, 0)
        s = f.read()
    print 'data size is %d' % len(s)
    return s


def createbmp():
    data = getdata('001.wav')
    with open('001.ppm', 'wb') as f:
        f.write('P6\n# comment\n640 480\n255\n')
        for v in reversed(data):
            f.write(str(v))


if __name__ == '__main__':
    createbmp()
