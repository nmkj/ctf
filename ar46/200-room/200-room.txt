#200 room
Chiho is finding Yui. Please help her!
Flag prefix is "AR46_"

$ nc room.ctf.nanimodekinai.xyz 49530

You can use these things.
libc: http://room.ctf.nanimodekinai.xyz:40080/libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so
service: http://room.ctf.nanimodekinai.xyz:40080/room-9d706208d63408253151e1abe72644d7c4a37cc0

$ echo -n [FLAG] | sha1sum
7f080192f60dc63448419dec29c866c3793e4bb3  -

----
Mirror files
[libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so]
[room-9d706208d63408253151e1abe72644d7c4a37cc0]


design: uma_dolce
