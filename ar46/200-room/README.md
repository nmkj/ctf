# ar46-ctf \#200 room

###### tags: `ar46` `ctf` `pwn`

Chiho is finding Yui. Please help her!  
Flag prefix is "AR46_"

You can use these things.

+ [libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so](https://bitbucket.org/uma_dolce/ctf/raw/master/ar46/200-room/libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so)
+ [room-9d706208d63408253151e1abe72644d7c4a37cc0](https://bitbucket.org/uma_dolce/ctf/raw/master/ar46/200-room/room-9d706208d63408253151e1abe72644d7c4a37cc0)

```
$ echo -n [FLAG] | sha1sum
9434b76a6c0c8505b0ee0defb40c255e6ee7ac66  -
```

Designed by [@nmkj_io](https://twitter.com/nmkj_io)
