#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 64

__attribute__((constructor)) void init() { setbuf(stdout, NULL); }

void greet() {
  puts("Hi! I'm Chiho Aikawa. What's your name?");
  printf("Name : ");

  char buf[BUFSIZE] = {0};
  fgets(buf, BUFSIZE, stdin);

  printf(buf);
  printf(", will you help me?\n");
  printf("I saw Ichii a little while ago and want to talk with her :)\n"
         "What room did she go?\n");
}

int getnumber() {
  int num;

  char buf[BUFSIZE] = {0};
  fgets(buf, BUFSIZE, stdin);
  num = atoi(buf);

  return num;
}

int main(void) {
  int room;

  greet();

  for (int i = 0; i < 3; i++) {
    printf("Room number : ");
    room = getnumber();

    if (i < 2) {
      printf("\nI couldn't find her at room %d :(\n"
             "Can you tell me another one?\n",
             room);
    } else {
      printf("\nI couldn't find her at room %d :(\n\n", room);
    }
  }

  puts("Uhhh... Sorry, I'm beat. She may have already gone home...");
  puts("Thank you. I'll try another day. Goodbye ;)");

  return 0;
}
