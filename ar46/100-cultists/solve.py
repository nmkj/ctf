import sys
import hashlib
from itertools import product

# SETUNAILO ?

key = '\x74\x61\x03\x03\x0a\x17\x61\x68\x19\x63\x7d\x02\x7c\x04\x0a\x7f\x67\x6e\x7b\x78\x06\x6c\x70\x67\x10\x7a\x60\x02\x1f\x64\x6a\x06\x74\x67\x64\x1e\x60\x64\x1e\x03'

target = '33b34995357827a5980566e0014936fb236e3a0e'
flag_prefix = 'AR46_Y'

chars = [chr(i) for i in range(33, 126+1)]

ans_prefix = '5375U' # AR46_
N = 'N^'

def xor_two_str(a,b):
  a *= 4
  xored = []
  for i, v in enumerate(a):
    xor = ord(v) ^ ord(key[i])
    xored.append(chr(xor))
  return ''.join(xored)

def check(chars, repeat):
  pws = product(chars, repeat=repeat)
  for n in N:
    for pw in pws:
      res = ''.join(pw)
      string = ans_prefix + n + res
      hashhex = hashlib.sha1(xor_two_str(string, key)).hexdigest()
      if hashhex == target:
        return string

result = check(chars, 4)

if result is None:
  print 'fail'
else:
  print 'got it: {}'.format(result)
